import re

from helpers import get_lines_from_file

DAY = 3

part_number_pattern = re.compile(r"\d+")
gear_pattern = re.compile(r"\*")


def get_search_box(line_number: int, start: int, end: int, line_count: int) -> tuple[list[int], int, int]:
    start = start - 1 if start > 0 else start
    end = end + 1 if end < line_count else end
    line_numbers = [i for i in (line_number - 1, line_number, line_number + 1) if 0 <= i < line_count]
    return line_numbers, start, end


def validate_part_number(line_number: int, start: int, end: int, lines: list[str]) -> bool:
    line_numbers, start, end = get_search_box(line_number, start, end, len(lines))

    return any(c != "." and not c.isdigit() for i in line_numbers for c in lines[i][start:end])


def get_part_numbers(lines: list[str]) -> list[int]:
    part_numbers = []

    for line_number, line in enumerate(lines):
        matches = part_number_pattern.finditer(line)
        for m in matches:
            if validate_part_number(line_number, m.start(), m.end(), lines):
                part_numbers.append(int(m.group()))

    return part_numbers


def get_gear_parts(line_number: int, gear_index: int, lines: list[str]) -> list[int]:
    line_numbers, start, end = get_search_box(line_number, gear_index, gear_index + 1, len(lines))
    gear_parts = []

    for i in line_numbers:
        if any(c.isdigit() for c in lines[i][start:end]):
            gear_parts.extend(
                [
                    int(m.group())
                    for m in part_number_pattern.finditer(lines[i])
                    if m.start() < end and m.end() > start
                ]
            )

    return gear_parts


def get_gear_ratios(lines: list[str]) -> list[int]:
    gear_ratios = []

    for line_number, line in enumerate(lines):
        matches = gear_pattern.finditer(line)
        for m in matches:
            gear_parts = get_gear_parts(line_number, m.start(), lines)
            if len(gear_parts) == 2:
                gear_ratios.append(gear_parts[0] * gear_parts[1])

    return gear_ratios


def run1() -> int:
    part_numbers = get_part_numbers(get_lines_from_file(DAY))
    return sum(part_numbers)


def run2() -> int:
    gear_ratios = get_gear_ratios(get_lines_from_file(DAY))
    return sum(gear_ratios)
