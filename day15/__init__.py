from typing import Optional

from helpers import get_lines_from_file

DAY = 15


def hash_string(s: str) -> int:
    res = 0
    for c in s:
        res += ord(c)
        res *= 17
        res %= 256
    return res


def find_lense(box: list[tuple[str, int]], label: str) -> Optional[int]:
    return next((i for i, (l, _) in enumerate(box) if l == label), None)


def put_lenses_in_boxes(sequence: list[str]) -> list[list[tuple[str, int]]]:
    boxes = [[] for _ in range(256)]

    for s in sequence:
        if s[-1] == "-":
            label = s[:-1]
            box_index = hash_string(label)
            lense_index = find_lense(boxes[box_index], label)

            if lense_index is not None:
                del boxes[box_index][lense_index]
        else:
            focal_length = int(s[-1])
            label = s[:-2]
            box_index = hash_string(label)
            lense_index = find_lense(boxes[box_index], label)

            if lense_index is not None:
                boxes[box_index][lense_index] = (label, focal_length)
            else:
                boxes[box_index].append((label, focal_length))

    return boxes


def run1() -> int:
    sequence = get_lines_from_file(DAY)[0].split(",")
    return sum(hash_string(s) for s in sequence)


def run2() -> int:
    sequence = get_lines_from_file(DAY)[0].split(",")
    boxes = put_lenses_in_boxes(sequence)
    return sum(
        box_number * lense_number * focal_length
        for box_number, box in enumerate(boxes, start=1)
        for lense_number, (_, focal_length) in enumerate(box, start=1)
    )
