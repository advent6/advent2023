import math
import time
from collections import defaultdict

from helpers import get_lines_from_file, recursionlimit

DAY = 17


def find_shortest_path(all_blocks: tuple[tuple[float]]) -> float:
    max_height = len(all_blocks)
    max_width = len(all_blocks[0])
    known_shortest_paths = [[defaultdict(lambda: [math.inf, []]) for _ in range(max_width)] for _ in range(max_height)]
    start_point = (0, 0)
    end_point = (max_height - 1, max_width - 1)
    known_shortest_paths[start_point[0]][start_point[1]][()] = [0, []]
    blocks = ()

    def check_point(path: list[tuple[int, int]], new_path_length: float, recent_path: list[tuple[int, int]]):
        point = path[-1]

        known_shortest_paths[point[0]][point[1]][tuple(recent_path)] = [new_path_length, path]

        if point == end_point:
            return

        possible_moves = []
        for i, j in ((0, 1), (1, 0), (0, -1), (-1, 0)):
            if 0 <= point[0] + i < max_height and 0 <= point[1] + j < max_width and recent_path.count((i, j)) < 3:
                possible_moves.append((i, j))

        for move in possible_moves:
            next_point = (point[0] + move[0], point[1] + move[1])
            if next_point in path:
                continue

            known_paths = known_shortest_paths[next_point[0]][next_point[1]]
            next_recent_path = ([move] + recent_path[:next((i for i, m in enumerate(recent_path) if m != move), 3)])[:3]
            next_path_length = new_path_length + blocks[next_point[0]][next_point[1]]

            known_path_length, known_path = known_paths[tuple(next_recent_path)]
            if known_path_length < next_path_length:
                continue

            new_path = path + [next_point]
            if known_path_length == next_path_length and known_path != new_path:
                continue

            check_point(new_path, next_path_length, next_recent_path)

    for i in range(1, len(all_blocks)):
        end_point = (i, i)
        blocks = tuple([row[:i + 1] for row in all_blocks[:i + 1]])
        max_height = i + 1
        max_width = i + 1
        check_point([(0, 0)], 0, [])

    return min(known_shortest_paths[end_point[0]][end_point[1]].values(), key=lambda x: x[0])[0]


def find_shortest_path_ultra(all_blocks: tuple[tuple[float]]) -> float:
    max_height = len(all_blocks)
    max_width = len(all_blocks[0])
    known_shortest_paths = [[defaultdict(lambda: [math.inf, []]) for _ in range(max_width)] for _ in range(max_height)]
    start_point = (0, 0)
    end_point = (max_height - 1, max_width - 1)
    known_shortest_paths[start_point[0]][start_point[1]][()] = [0, []]
    blocks = ()
    possible_transformations = (
        (0, 4), (0, 5), (0, 6), (0, 7),
        (4, 0), (5, 0), (6, 0), (7, 0),
        (0, -4), (0, -5), (0, -6), (0, -7),
        (-4, 0), (-5, 0), (-6, 0), (-7, 0),
    )

    def check_point(path: list[tuple[int, int]], new_path_length: float, recent_path: list[tuple[int, int]]):
        point = path[-1]
        known_shortest_paths[point[0]][point[1]][tuple(recent_path)] = [new_path_length, path]

        if point == end_point:
            return

        possible_moves = []
        for i, j in possible_transformations:
            if 0 <= point[0] + i < max_height and 0 <= point[1] + j < max_width and recent_path.count((i, j)) < 6:
                possible_moves.append((i, j))

        for move in possible_moves:
            next_point = (point[0] + move[0], point[1] + move[1])
            if next_point in path:
                continue
            known_paths = known_shortest_paths[next_point[0]][next_point[1]]
            next_recent_path = list(recent_path)
            next_path_length = new_path_length
            new_path = list(path)
            for ii in range(abs(move[0]) + 1):
                for jj in range(abs(move[1]) + 1):
                    if ii == 0 and jj == 0:
                        continue
                    i = 0 if ii == 0 else ii * (move[0] // abs(move[0]))
                    j = 0 if jj == 0 else jj * (move[1] // abs(move[1]))
                    next_recent_path = [(0 if i == 0 else i // abs(i), 0 if j == 0 else j // abs(j))] + next_recent_path
                    middle_point = (point[0] + i, point[1] + j)
                    next_path_length += blocks[middle_point[0]][middle_point[1]]
                    new_path.append(middle_point)
            # next_recent_path = next_recent_path[:10]
            next_recent_path = next_recent_path[
                :next((i for i, m in enumerate(next_recent_path) if m != next_recent_path[0]), 10)
            ]
            if len(next_recent_path) > 9 and len(set(next_recent_path)) < 2:
                continue
            known_path_length, known_path = known_paths[tuple(next_recent_path)]
            if known_path_length < next_path_length:
                continue
            if known_path_length == next_path_length and known_path != new_path:
                continue

            check_point(new_path, next_path_length, next_recent_path)

    print("Dimensions", max_height, max_width)
    total_time1 = time.time()
    # blocks = all_blocks
    # check_point([(0, 0)], 0)
    for i in range(4, len(all_blocks)):
        end_point = (i, i)
        blocks = tuple([row[:i + 1] for row in all_blocks[:i + 1]])
        max_height = i + 1
        max_width = i + 1
        print("Checking", end_point)
        t1 = time.time()
        check_point([(0, 0)], 0, [])
        t2 = time.time()
        print("Time", t2 - t1)
    total_time2 = time.time()
    print("Total time", total_time2 - total_time1)

    # for i in range(max_height):
    #     row = []
    #     for j in range(max_width):
    #         row.append(min([v[0] for v in known_shortest_paths[i][j].values()]))
    #     print(" ".join([f"{row[j]} ({blocks[i][j]})".rjust(10) for j in range(max_width)]))

    # print(min(known_shortest_paths[end_point[0]][end_point[1]].values(), key=lambda x: x[0]))
    return min(known_shortest_paths[end_point[0]][end_point[1]].values(), key=lambda x: x[0])[0]


def run1() -> float:
    blocks = tuple(get_lines_from_file(DAY, file_name="input.txt", type_=lambda l: tuple(map(int, l))))
    with recursionlimit(1000000):  # probably not necessary after optimizations, but I'm too scared to remove it
        return find_shortest_path(blocks)


def run2() -> float:
    # WOULD WORK FOR DAYS ON REAL DATA, ANSWER NOT SUBMITTED YET
    blocks = tuple(get_lines_from_file(DAY, file_name="input.txt", type_=lambda l: tuple(map(int, l))))
    with recursionlimit(10000000):
        try:
            return find_shortest_path_ultra(blocks)
        except RecursionError:
            import sys
            print("RecursionError", sys.getrecursionlimit())
