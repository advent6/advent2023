from shapely.geometry import LineString, MultiLineString, Polygon

from helpers import get_lines_from_file

DAY = 18

TRANSFORMATIONS = {
    "R": (0, 1),
    "L": (0, -1),
    "U": (-1, 0),
    "D": (1, 0),
}

COLOR_DIRECTIONS = {
    "0": "R",
    "1": "D",
    "2": "L",
    "3": "U",
}


def parse_line(line: str) -> tuple[str, int]:
    direction, count, _ = line.split()
    return direction, int(count)


def parse_color(line: str) -> tuple[str, int]:
    color = line.split()[2]
    return COLOR_DIRECTIONS[color[-2]], int(color[2:7], 16)


def get_vertices(dig_plan: list[tuple[str, int, str]]) -> list[tuple[int, int]]:
    current_position = (0, 0)
    tiles = [current_position]
    for direction, count in dig_plan:
        transformation = TRANSFORMATIONS[direction]
        current_position = (
            current_position[0] + transformation[0] * count,
            current_position[1] + transformation[1] * count,
        )
        tiles.append(current_position)

    return tiles


def get_dug_count(dig_plan: list[tuple[str, int, str]]) -> int:
    vertices = get_vertices(dig_plan)
    p = Polygon(vertices)
    min_i, min_j, max_i, max_j = p.bounds

    # For some reason I wasn't able to make it work with calculating the polygon area with shoelace formula
    count = 0
    for i in range(int(min_i), int(max_i) + 1):
        line = LineString([(i, min_j), (i, max_j)])
        intersection = line.intersection(p)

        if isinstance(intersection, MultiLineString):
            add_count = 1
            for i, geom in enumerate(list(intersection.geoms)[1:], start=1):
                if not geom.touches(intersection.geoms[i - 1]):
                    add_count += 1
        else:
            add_count = 1

        count += (line.intersection(p).length + add_count)

    return int(count)


def run1() -> int:
    dig_plan = get_lines_from_file(DAY, type_=parse_line)
    return get_dug_count(dig_plan)


def run2() -> int:
    dig_plan = get_lines_from_file(DAY, type_=parse_color)
    return get_dug_count(dig_plan)
