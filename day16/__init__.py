from collections import defaultdict

from helpers import get_lines_from_file

DAY = 16

TRANSFORMATIONS = {
    "R": (0, 1),
    "L": (0, -1),
    "U": (-1, 0),
    "D": (1, 0),
}

MIRRORS = {
    "/": {
        "R": "U",
        "L": "D",
        "U": "R",
        "D": "L",
    },
    "\\": {
        "R": "D",
        "L": "U",
        "U": "L",
        "D": "R",
    },
}

SPLITTERS = {
    "-": {
        "R": ("R",),
        "L": ("L",),
        "U": ("L", "R"),
        "D": ("L", "R"),
    },
    "|": {
        "R": ("U", "D"),
        "L": ("U", "D"),
        "U": ("U",),
        "D": ("D",),
    },
}


def get_energized_tiles_count(grid: tuple[tuple[str]], starting_beam: tuple[tuple, str] = ((0,0), "R")) -> int:
    max_height = len(grid)
    max_width = len(grid[0])
    energized_tiles = defaultdict(list)

    beams = [starting_beam]

    while beams:
        coordinates, direction = beams.pop()
        moves = []
        tile = grid[coordinates[0]][coordinates[1]]

        if direction in energized_tiles[coordinates]:
            continue
        energized_tiles[coordinates].append(direction)

        if tile == ".":
            moves.append(direction)
        elif tile in MIRRORS:
            moves.append(MIRRORS[tile][direction])
        else:  # tile in SPLITTERS
            moves.extend(SPLITTERS[tile][direction])

        for new_direction in moves:
            move = TRANSFORMATIONS[new_direction]
            new_coordinates = (coordinates[0] + move[0], coordinates[1] + move[1])
            if 0 <= new_coordinates[0] < max_height and 0 <= new_coordinates[1] < max_width:
                beams.append((new_coordinates, new_direction))

    return len(energized_tiles)


def run1() -> int:
    grid = tuple(get_lines_from_file(DAY, type_=tuple))
    return get_energized_tiles_count(grid)


def run2() -> int:
    grid = tuple(get_lines_from_file(DAY, type_=tuple))
    max_height = len(grid)
    max_width = len(grid[0])

    starting_beams = []
    for i in range(max_height):
        starting_beams.append(((i, 0), "R"))
        starting_beams.append(((i, max_width - 1), "L"))
    for j in range(max_width):
        starting_beams.append(((0, j), "D"))
        starting_beams.append(((max_height - 1, j), "U"))

    return max(get_energized_tiles_count(grid, starting_beam) for starting_beam in starting_beams)

