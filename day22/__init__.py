from functools import lru_cache

from shapely.geometry import LineString

from helpers import get_lines_from_file

DAY = 22


class Brick:
    def __init__(self, start: list[int, int, int], end: list[int, int, int]):
        self.start = start
        self.end = end
        if start[0] == end[0] and start[1] == end[1]:
            self.linestring = LineString([start, [end[0] + 0.1, end[1] + 0.1, end[2]]])
        else:
            self.linestring = LineString([start, end])
        self.supporting_bricks = []


def parse_input(lines: list[str]) -> list[Brick]:
    bricks = []

    for i, line in enumerate(lines):
        start_str, end_str = line.split("~")
        bricks.append(
            Brick(
                list(map(int, start_str.split(","))),
                list(map(int, end_str.split(","))),
            ),
        )

    return bricks


def lower_bricks(bricks: list[Brick]) -> list[Brick]:
    lowered_bricks = []
    current_z = 1

    for brick in sorted(bricks, key=lambda brick: min(brick.start[2], brick.end[2])):
        if brick.start[2] >= brick.end[2] > current_z:
            brick.start[2], brick.end[2] = brick.start[2] - (brick.end[2] - current_z), current_z
        elif brick.end[2] > brick.start[2] > current_z:
            brick.start[2], brick.end[2] = current_z, brick.end[2] - (brick.start[2] - current_z)

        while True:
            supporting_bricks = [
                b
                for b in lowered_bricks
                if (
                    b.linestring.intersects(brick.linestring)
                    and min(brick.start[2], brick.end[2]) == max(b.start[2], b.end[2])
                )
            ]

            if supporting_bricks or brick.start[2] == 0 or brick.end[2] == 0:
                brick.supporting_bricks = supporting_bricks
                brick.start[2] += 1
                brick.end[2] += 1
                lowered_bricks.append(brick)
                break

            brick.start[2] -= 1
            brick.end[2] -= 1

        current_z = max(current_z, brick.start[2], brick.end[2])

    return lowered_bricks


@lru_cache(maxsize=100000)
def get_all_supported_bricks(removed_bricks: tuple[Brick], bricks: tuple[Brick]) -> list[Brick]:
    all_supported_bricks = [
        b
        for b in bricks
        if b not in removed_bricks and b.supporting_bricks and all(b in removed_bricks for b in b.supporting_bricks)
    ]

    if not all_supported_bricks:
        return []

    all_supported_bricks.extend(get_all_supported_bricks(tuple(list(removed_bricks) + all_supported_bricks), bricks))
    return all_supported_bricks


def run1() -> int:
    bricks = parse_input(get_lines_from_file(DAY))
    lowered_bricks = lower_bricks(bricks)
    return len([brick for brick in lowered_bricks if not [b for b in lowered_bricks if b.supporting_bricks == [brick]]])


def run2() -> int:
    bricks = parse_input(get_lines_from_file(DAY))
    lowered_bricks = lower_bricks(bricks)
    return sum(len(get_all_supported_bricks(tuple([brick]), tuple(lowered_bricks))) for brick in lowered_bricks)

