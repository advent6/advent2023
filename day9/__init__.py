from helpers import get_lines_from_file

DAY = 9


def parse_input(file_name: str = "input.txt") -> list[tuple[int]]:
    return get_lines_from_file(DAY, file_name=file_name, type_=lambda l: tuple(map(int, l.split())))


def reduce_sequence(sequence: tuple[int]) -> tuple[int]:
    return tuple(sequence[i] - sequence[i - 1] for i in range(1, len(sequence)))


def get_next_sequence_value(sequence: tuple[int]) -> int:
    if len(set(sequence)) == 1:
        return sequence[0]
    return sequence[-1] + get_next_sequence_value(reduce_sequence(sequence))


def get_previous_sequence_value(sequence: tuple[int]) -> int:
    if len(set(sequence)) == 1:
        return sequence[0]
    return sequence[0] - get_previous_sequence_value(reduce_sequence(sequence))


def run1() -> int:
    sequences = parse_input()
    return sum(get_next_sequence_value(sequence) for sequence in sequences)


def run2() -> int:
    sequences = parse_input()
    return sum(get_previous_sequence_value(sequence) for sequence in sequences)
