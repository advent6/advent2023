from typing import Optional, Callable

from more_itertools import split_at

from helpers import get_lines_from_file

DAY = 13


def find_reflection_index(pattern: list[str]) -> Optional[int]:
    pattern_length = len(pattern)

    for i in range(pattern_length - 1):
        if pattern[i] == pattern[i + 1]:
            is_reflection = (
                i == 0
                or all(pattern[i + j + 1] == pattern[i - j] for j in range(1, min(i + 1, pattern_length - i - 1)))
            )

            if is_reflection:
                return i + 1


def find_smudged_reflection_index(pattern: list[str]) -> Optional[int]:
    pattern_length = len(pattern)
    for i in range(pattern_length - 1):
        smudge_count = 0

        def compare_lines(a, b):
            nonlocal smudge_count

            if a == b:
                return True

            if smudge_count == 0 and len([i for i in range(len(a)) if a[i] != b[i]]) == 1:
                smudge_count += 1
                return True
            return False

        if compare_lines(pattern[i], pattern[i + 1]):
            is_reflection = (
                i == 0
                or all(
                    compare_lines(pattern[i + j + 1], pattern[i - j])
                    for j in range(1, min(i + 1, pattern_length - i - 1))
                )
            )

            if is_reflection and smudge_count == 1:
                return i + 1


def get_mirror_value(pattern: list[str], find_func: Callable) -> int:
    horizontal_reflection_index = find_func(pattern)
    if horizontal_reflection_index is not None:
        return 100 * horizontal_reflection_index

    transposed_pattern = list(map(lambda s: ''.join(s), zip(*pattern)))
    return find_func(transposed_pattern)


def run1() -> int:
    patterns = split_at(get_lines_from_file(DAY, file_name="input.txt"), lambda l: l is None)
    return sum(get_mirror_value(pattern, find_func=find_reflection_index) for pattern in patterns)


def run2() -> int:
    patterns = split_at(get_lines_from_file(DAY, file_name="input.txt"), lambda l: l is None)
    return sum(get_mirror_value(pattern, find_func=find_smudged_reflection_index) for pattern in patterns)
