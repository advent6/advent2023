import math
import re

from helpers import get_lines_from_file

DAY = 6


def parse_input(lines: list[str]) -> list[tuple[int, int]]:
    number_regex = re.compile(r"\d+")
    return list(zip(*[map(int, number_regex.findall(line)) for line in lines]))


def parse_second_input(lines: list[str]) -> tuple[int, int]:
    time = int(''.join(c for c in lines[0] if c.isdigit()))
    distance = int(''.join(c for c in lines[1] if c.isdigit()))
    return time, distance


def get_winning_combos_count(time: int, distance: int) -> int:
    winning_count = 0
    # Afterthought: could have just do winning_count = winning_count * 2 - 1 instead of going through -1, 1
    for i, j in ((1, -1), (-1, 1)):
        press_time = time // 2
        move_time = time - press_time
        while press_time * move_time > distance:
            winning_count += 1
            press_time += i
            move_time += j

    # assume there's always a way to win
    return winning_count - 1


def run1() -> int:
    races = parse_input(get_lines_from_file(DAY))
    return math.prod(get_winning_combos_count(time, distance) for time, distance in races)


def run2() -> int:
    time, distance = parse_second_input(get_lines_from_file(DAY))
    return get_winning_combos_count(time, distance)

