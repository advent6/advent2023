from itertools import combinations
from typing import Iterator

from helpers import get_lines_from_file

DAY = 11


def get_empty_rows_and_columns(universe: list[list[str]]) -> tuple[list[int], list[int]]:
    empty_row_indices = [i for i, row in enumerate(universe) if all(c == "." for c in row)]
    empty_column_indices = [
        j for j in range(len(universe[0])) if all(row[j] == "." for row in universe)
    ]
    return empty_row_indices, empty_column_indices


def iterate_galaxies(universe: list[list[str]]) -> Iterator[tuple[int, int]]:
    for i in range(len(universe)):
        for j in range(len(universe[0])):
            if universe[i][j] == "#":
                yield i, j


def get_shortest_path(point: tuple[int, int], other_point: tuple[int, int]) -> int:
    return abs(point[0] - other_point[0]) + abs(point[1] - other_point[1])


def get_shortest_path_between_galaxies(
        point: tuple[int, int],
        other_point: tuple[int, int],
        empty_row_indices: list[int],
        empty_column_indices: list[int],
        time_multiplier: int
) -> int:
    steps = get_shortest_path(point, other_point)

    rows_range = range(*sorted([point[0], other_point[0]]))
    columns_range = range(*sorted([point[1], other_point[1]]))

    empty_row_count = len([i for i in empty_row_indices if i in rows_range])
    empty_column_count = len([j for j in empty_column_indices if j in columns_range])

    if empty_row_count:
        steps += (empty_row_count * (time_multiplier - 1))
    if empty_column_count:
        steps += (empty_column_count * (time_multiplier - 1))

    return steps


def get_path_sum(time_multiplier: int, file_name: str = "input.txt") -> int:
    universe = get_lines_from_file(DAY, file_name=file_name, type_=list)
    empty_row_indices, empty_column_indices = get_empty_rows_and_columns(universe)

    return sum(
        get_shortest_path_between_galaxies(
            galaxy, other_galaxy, empty_row_indices, empty_column_indices, time_multiplier
        )
        for galaxy, other_galaxy in combinations(iterate_galaxies(universe), 2)
    )


def run1() -> int:
    return get_path_sum(2)


def run2() -> int:
    return get_path_sum(1000000)
