import math
from collections import defaultdict

from helpers import get_lines_from_file

DAY = 2


def parse_game(line: str) -> dict:
    game_id, rounds_str = line[5:].split(": ", 2)
    game = {"id": int(game_id), "rounds": []}

    for round_str in rounds_str.split("; "):
        game_round = {"blue": 0, "red": 0, "green": 0}
        for cubes_str in round_str.split(", "):
            cnt, color = cubes_str.split(" ", 2)
            game_round[color] += int(cnt)

        game["rounds"].append(game_round)

    return game


def validate_game(game: dict, max_values: dict[str, int]):
    return not any(game_round[color] > max_values[color] for game_round in game["rounds"] for color in game_round )


def get_minimal_game_power(game: dict) -> int:
    max_by_colors = defaultdict(int)
    for game_round in game["rounds"]:
        for color, cnt in game_round.items():
            max_by_colors[color] = max(max_by_colors[color], game_round[color])

    return math.prod(max_by_colors.values())


def run1() -> int:
    max_values = {"blue": 14, "red": 12, "green": 13}
    games = [parse_game(line) for line in get_lines_from_file(DAY)]

    return sum(game["id"] for game in games if validate_game(game, max_values))


def run2() -> int:
    games = [parse_game(line) for line in get_lines_from_file(DAY)]
    return sum(get_minimal_game_power(game) for game in games)
