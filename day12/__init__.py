import re
from functools import lru_cache

from helpers import get_lines_from_file

DAY = 12
POTENTIAL_BROKEN_PATTERN = re.compile(r"[\?#]+")


def parse_input(lines: list[str]) -> list[tuple[str, tuple[int]]]:
    res = []

    for line in lines:
        springs, broken_groups_str = line.split(" ", 2)
        res.append((springs, tuple(map(int, broken_groups_str.split(",")))))

    return res


@lru_cache(maxsize=1000000)
def get_combo_count(springs: str, broken_groups: tuple[int]) -> int:
    group_length = broken_groups[0]

    max_index = springs.find("#")
    if max_index == -1:
        max_index = len(springs) - 1
    else:
        limit = min(len(springs) - 1, max_index + group_length - 1)
        while max_index < limit and springs[max_index] in ("#", "?"):
            max_index += 1

    check_str = springs[:max_index + 1]
    slots = list(POTENTIAL_BROKEN_PATTERN.finditer(check_str))
    if not slots:
        return 0

    min_remaining_length = None
    min_remaining_places = None
    if len(broken_groups) > 1:
        min_remaining_places = sum(broken_groups[1:])
        min_remaining_length = min_remaining_places + len(broken_groups) - 1

    combo_count = 0
    for slot in slots:
        slot_length = slot.end() - slot.start()
        if slot_length < group_length:
            continue

        for i in range(slot_length - group_length + 1):
            tail = springs[slot.start() + i + group_length:]

            if len(broken_groups) == 1:
                if tail.find("#") != -1:
                    continue
                combo_count += 1
            else:
                if min_remaining_length is not None and len(tail) < min_remaining_length:
                    continue
                if min_remaining_places is not None and tail.count("#") + tail.count("?") < min_remaining_places:
                    continue
                if tail.startswith("#"):
                    continue
                if tail.startswith("?"):
                    tail = tail[1:]
                combo_count += get_combo_count(tail, broken_groups[1:])

    return combo_count


def unfold_springs(springs: str, broken_groups: tuple[int], multiplier=5) -> tuple[str, tuple[int]]:
    return "?".join([springs] * multiplier), broken_groups * multiplier


def run1() -> int:
    data = parse_input(get_lines_from_file(DAY))
    return sum(get_combo_count(springs, broken_groups) for springs, broken_groups in data)


def run2() -> int:
    data = [
        unfold_springs(springs, broken_groups)
        for springs, broken_groups in parse_input(get_lines_from_file(DAY))
    ]
    return sum(get_combo_count(springs, broken_groups) for springs, broken_groups in data)
