from collections import Counter
from enum import Enum
from functools import cached_property, total_ordering

from helpers import get_lines_from_file

DAY = 7


class CardType(int, Enum):
    HIGH_CARD = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE_OF_A_KIND = 3
    FULL_HOUSE = 4
    FOUR_OF_A_KIND = 5
    FIVE_OF_A_KIND = 6


@total_ordering
class Card:
    CARD_VALUES = "23456789TJQKA"

    def __init__(self, value: str):
        self.value = value

    @cached_property
    def strength(self):
        return self.CARD_VALUES.index(self.value)

    def __eq__(self, other):
        if isinstance(other, str):
            return self.value == other
        return self.value == other.value

    def __lt__(self, other):
        return self.strength < other.strength

    def __hash__(self):
        return hash(self.value)


class CardWithJoker(Card):
    CARD_VALUES = "J23456789TQKA"


@total_ordering
class Hand:
    card_type_class = Card

    def __init__(self, cards: str, bid: int):
        self.bid = bid
        self.cards = list(map(self.card_type_class, cards))

    def get_card_counts(self) -> list[tuple]:
        return Counter(self.cards).most_common(5)

    @cached_property
    def type(self) -> CardType:
        card_counts = self.get_card_counts()
        match len(card_counts):
            case 1:
                return CardType.FIVE_OF_A_KIND
            case 2:
                return CardType.FOUR_OF_A_KIND if card_counts[0][1] == 4 else CardType.FULL_HOUSE
            case 3:
                return CardType.THREE_OF_A_KIND if card_counts[0][1] == 3 else CardType.TWO_PAIR
            case 4:
                return CardType.ONE_PAIR
            case 5:
                return CardType.HIGH_CARD

    def __eq__(self, other):
        return self.type == other.type and self.cards == other.cards

    def __lt__(self, other):
        return self.type < other.type or (self.type == other.type and self.cards < other.cards)


class HandWithJoker(Hand):
    card_type_class = CardWithJoker

    def get_card_counts(self) -> list[tuple]:
        joker_count = self.cards.count("J")
        card_counts = [c for c in super().get_card_counts() if c[0] != "J"]
        if joker_count and card_counts:
            card_counts[0] = (card_counts[0][0], card_counts[0][1] + joker_count)

        return card_counts or [("J", 5)]


def play_game(lines: list[str], hand_class):
    hands = [hand_class(cards, int(bid)) for cards, bid in map(str.split, lines)]
    hands.sort()
    return sum(i * hand.bid for i, hand in enumerate(hands, start=1))


def run1() -> int:
    return play_game(get_lines_from_file(DAY), Hand)


def run2() -> int:
    return play_game(get_lines_from_file(DAY), HandWithJoker)
