from functools import lru_cache

from helpers import get_lines_from_file, recursionlimit

DAY = 21

TRANSFORMATIONS = ((0, 1), (1, 0), (0, -1), (-1, 0))


def get_possible_end_positions(grid: list[list[str]], max_step_count: int) -> set[tuple[int, int]]:
    start_position = next((i, j) for i, row in enumerate(grid) for j, cell in enumerate(row) if cell == "S")
    max_height = len(grid)
    max_width = len(grid[0])
    end_positions = set()

    @lru_cache(maxsize=100000)
    def get_next_positions(position: tuple[int, int]) -> list[tuple[int, int]]:
        res = []
        for transformation in TRANSFORMATIONS:
            new_position = (position[0] + transformation[0], position[1] + transformation[1])
            if (
                    0 <= new_position[0] < max_height
                    and 0 <= new_position[1] < max_width
                    and grid[new_position[0]][new_position[1]] != "#"
            ):
                res.append(new_position)
        return res

    @lru_cache(maxsize=10000000)
    def step(from_position: tuple[int, int], step_count: 0):
        if step_count == max_step_count:
            end_positions.add(from_position)
            return

        for next_position in get_next_positions(from_position):
            step(next_position, step_count + 1)

    step(start_position, 0)
    return end_positions


def get_possible_end_positions_repeat(grid: list[list[str]], max_step_count: int) -> set[tuple[int, int]]:
    start_position = next((i, j) for i, row in enumerate(grid) for j, cell in enumerate(row) if cell == "S")
    max_height = len(grid)
    max_width = len(grid[0])
    end_positions = set()

    @lru_cache(maxsize=1000000)
    def get_next_positions(position: tuple[int, int]) -> list[tuple[int, int]]:
        res = []
        for transformation in TRANSFORMATIONS:
            new_position = (position[0] + transformation[0], position[1] + transformation[1])
            adjusted_position = new_position
            if not 0 <= adjusted_position[0] < max_height:
                adjusted_position = (adjusted_position[0] % max_height, adjusted_position[1])
            if not 0 <= adjusted_position[1] < max_width:
                adjusted_position = (adjusted_position[0], adjusted_position[1] % max_width)

            if grid[adjusted_position[0]][adjusted_position[1]] != "#":
                res.append(new_position)
        return res

    steps_to_check = [(start_position, 0)]
    from collections import deque
    # checked_combos = deque(maxlen=100000)
    checked_combos = set()
    i = 0
    while steps_to_check:
        i += 1
        if not i % 200000:
            print(steps_to_check[0], "stc", len(steps_to_check), "cc", len(checked_combos), "H", get_next_positions.cache_info().hits, "M", get_next_positions.cache_info().misses, "CS", get_next_positions.cache_info().currsize, "end", len(end_positions))
            # if not i % 400000:
            # checked_combos = set()

        position, step_count = steps_to_check.pop(0)
        if (position, step_count) in checked_combos:
            # print("Bingo")
            continue
        # checked_combos.append((position, step_count))
        checked_combos.add((position, step_count))

        # print(position, step_count)
        if step_count == max_step_count:
            end_positions.add(position)
            continue

        # steps_to_check.extend([(next_position, step_count + 1) for next_position in get_next_positions(position)])
        steps_to_check[:0] = [(next_position, step_count + 1) for next_position in get_next_positions(position)]

    # @lru_cache(maxsize=10000000)
    # def step(from_position: tuple[int, int], step_count: 0):
    #     if step_count == max_step_count:
    #         end_positions.add(from_position)
    #         return
    #
    #     for next_position in get_next_positions(from_position):
    #         step(next_position, step_count + 1)
    #
    # step(start_position, 0)
    return end_positions


def run1() -> int:
    grid = get_lines_from_file(DAY, type_=list)
    end_positions = get_possible_end_positions(grid, 64)
    return len(end_positions)


def run2() -> int:
    # Doesn't work in reasonable time yet, answer not submitted
    grid = get_lines_from_file(DAY, file_name="test.txt", type_=list)
    import time
    t1 = time.time()
    end_positions = get_possible_end_positions_repeat(grid, 1000)
    t2 = time.time()
    print(t2 - t1)
    return len(end_positions)
