from typing import Sequence

from helpers import get_lines_from_file

DAY = 14

TILT_TRANSFORMATIOMS = {
    "N": -1,
    "S": 1,
    "W": -1,
    "E": 1,
}


def tilt_platform(platform: Sequence[Sequence[str]], tilt: str) -> tuple[tuple[str]]:
    platform = [list(row) for row in platform]
    transformation = TILT_TRANSFORMATIOMS[tilt]
    row_length = len(platform[0])
    row_count = len(platform)

    # I know this doesn't need to be if-else, but I don't have the spirit to generalize it
    # Also, could be better to calculate the map of obstacles once and move the stones between them instead of
    # going one by one
    if tilt in ("N", "S"):
        for j in range(row_length):
            rolling_stone_range = range(row_count) if tilt == "N" else range(row_count - 1, -1, -1)
            rolling_stone_indexes = [i for i in rolling_stone_range if platform[i][j] == "O"]

            for i in rolling_stone_indexes:
                current_index = i
                next_index = current_index + transformation

                while 0 <= next_index < row_count and platform[next_index][j] == ".":
                    current_index = next_index
                    next_index = current_index + transformation

                platform[i][j] = "."
                platform[current_index][j] = "O"
    else:
        for i in range(len(platform)):
            rolling_stone_range = range(row_length) if tilt == "W" else range(row_length - 1, -1, -1)
            rolling_stone_indexes = [j for j in rolling_stone_range if platform[i][j] == "O"]

            for j in rolling_stone_indexes:
                current_index = j
                next_index = current_index + transformation

                while 0 <= next_index < row_length and platform[i][next_index] == ".":
                    current_index = next_index
                    next_index = current_index + transformation

                platform[i][j] = "."
                platform[i][current_index] = "O"

    return tuple([tuple(row) for row in platform])


def get_load_on_north(platform: Sequence[Sequence[str]]) -> int:
    row_count = len(platform)
    return sum(row.count("O") * (row_count - i) for i, row in enumerate(platform))


def spin(platform: tuple[tuple[str]]) -> tuple[tuple[str]]:
    for tilt in ("N", "W", "S", "E"):
        platform = tilt_platform(platform, tilt)
    return platform


def get_cycle(platform: tuple[tuple[str]]) -> tuple[int, int, tuple[tuple[str]]]:
    seen = []
    i = 0

    while True:
        if platform in seen:
            cycle_start = seen.index(platform)
            return cycle_start, i - cycle_start, platform

        seen.append(platform)
        platform = spin(platform)
        i += 1


def spin_cycle(platform: tuple[tuple[str]], times: int) -> tuple[tuple[str]]:
    cycle_start, cycle_length, platform = get_cycle(platform)
    # will not work for times < cycle_start, but we don't need that
    times -= cycle_start

    for _ in range(times % cycle_length):
        platform = spin(platform)

    return platform


def run1() -> int:
    platform = tuple(get_lines_from_file(DAY, type_=tuple))
    platform = tilt_platform(platform, "N")
    return get_load_on_north(platform)


def run2() -> int:
    platform = tuple(get_lines_from_file(DAY, type_=tuple))
    platform = spin_cycle(platform, 1000000000)
    return get_load_on_north(platform)
