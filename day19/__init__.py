import math
import operator
from itertools import chain
from typing import Optional

from more_itertools import split_at

from helpers import get_lines_from_file

DAY = 19


OPERATORS = {
    ">": operator.gt,
    "<": operator.lt
}

RANGE_GETTERS = {
    OPERATORS["<"]: lambda x: range(1, x),
    OPERATORS[">"]: lambda x: range(x + 1, 4001)
}


def parse_input(lines: list[Optional[str]]) -> tuple[dict[str, list[dict]], list[dict[str, str]]]:
    workflow_lines, parts_lines = split_at(lines, lambda l: l is None)
    workflows = {}

    for line in workflow_lines:
        key, conditions_str = line.split("{", 2)
        conditions_str = conditions_str[:-1]
        conditions = []

        for condition_str in conditions_str.split(","):
            condition_parts = condition_str.split(":")
            condition = {"operation": None, "next_step": None}

            if len(condition_parts) == 2:
                condition["next_step"] = condition_parts[1]
                condition["operation"] = {
                    "param": condition_parts[0][0],
                    "operator": OPERATORS[condition_parts[0][1]],
                    "value": int(condition_parts[0][2:]),
                }
            else:
                condition["next_step"] = condition_parts[0]

            conditions.append(condition)

        workflows[key] = conditions

    parts = []
    for line in parts_lines:
        part = {}
        for part_str in line.replace("{", "").replace("}", "").split(","):
            key, value = part_str.split("=", 2)
            part[key] = int(value)
        parts.append(part)

    return workflows, parts


def check_part(part: dict[str, str], workflows: dict[str, list[dict]]) -> bool:
    current_workflow_key = "in"
    while True:
        if current_workflow_key == "A":
            return True
        if current_workflow_key == "R":
            return False

        conditions = workflows[current_workflow_key]
        for condition in conditions:
            operation = condition["operation"]

            if operation is None:
                current_workflow_key = condition["next_step"]
                break

            if operation["operator"](part[operation["param"]], operation["value"]):
                current_workflow_key = condition["next_step"]
                break


def get_accepted_ranges(workflows: dict[str, list[dict]]) -> list[dict[str, range]]:
    accepted_ranges = []
    ranges_to_check = [("in", {key: range(1, 4001) for key in ("x", "m", "a", "s")})]

    while ranges_to_check:
        workflow_key, ranges = ranges_to_check.pop()

        if workflow_key == "A":
            accepted_ranges.append(ranges)
            continue
        if workflow_key == "R":
            continue

        for condition in workflows[workflow_key]:
            operation = condition["operation"]
            if operation is None:
                ranges_to_check.append((condition["next_step"], ranges))
                break

            condition_range = RANGE_GETTERS[operation["operator"]](operation["value"])
            current_range = ranges[operation["param"]]
            overlap_range = range(
                max(current_range.start, condition_range.start), min(current_range.stop, condition_range.stop)
            )
            if not overlap_range:
                continue

            follow_ranges = ranges.copy()
            follow_ranges[operation["param"]] = overlap_range
            ranges_to_check.append((condition["next_step"], follow_ranges))

            for continue_range in (
                range(current_range.start, overlap_range.start),
                range(overlap_range.stop, current_range.stop),
            ):
                if not continue_range:
                    continue
                continue_ranges = ranges.copy()
                continue_ranges[operation["param"]] = continue_range
                ranges_to_check.append((workflow_key, continue_ranges))
            break

    return accepted_ranges


def run1() -> int:
    workflows, parts = parse_input(get_lines_from_file(DAY))
    accepted_parts = [p for p in parts if check_part(p, workflows)]
    return sum(chain(*(p.values() for p in accepted_parts)))


def run2() -> int:
    workflows, _ = parse_input(get_lines_from_file(DAY))
    ranges = get_accepted_ranges(workflows)
    return sum(math.prod([len(r) for r in rng.values()]) for rng in ranges)
