from helpers import get_lines_from_file

DAY = 10

TRANSFORMATIONS = {
    "|": ((-1, 0), (1, 0)),
    "-": ((0, 1), (0, -1)),
    "L": ((0, 1), (-1, 0)),
    "J": ((-1, 0), (0, -1)),
    "7": ((1, 0), (0, -1)),
    "F": ((1, 0), (0, 1)),
}


def apply_transformation(position: tuple[int, int], transformation: tuple[int, int]) -> tuple[int, int]:
    return position[0] + transformation[0], position[1] + transformation[1]


def get_next_position(
        current_position: tuple[int, int], pipe: str, previous_position:tuple[int, int]
) -> tuple[int, int]:
    transformations = TRANSFORMATIONS[pipe]
    for transformation in transformations:
        if (position := apply_transformation(current_position, transformation)) != previous_position:
            return position


def check_adjacency(position: tuple[int, int], other: tuple[int, int], pipe: str) -> bool:
    return (
        pipe in TRANSFORMATIONS
        and any(apply_transformation(other, transformation) == position for transformation in TRANSFORMATIONS[pipe])
    )


def find_start_position(pipe_map: list[list[str]]) -> tuple[int, int]:
    return next((i, j) for i in range(len(pipe_map)) for j in range(len(pipe_map[0])) if pipe_map[i][j] == "S")


def get_loop(pipe_map: list[list[str]], start_position=None) -> list[tuple[int, int]]:
    start_position = start_position or find_start_position(pipe_map)
    previous_position = start_position

    for transformation in ((0, 1), (0, -1), (-1, 0), (1, 0)):
        position = apply_transformation(start_position, transformation)
        if check_adjacency(start_position, position, pipe_map[position[0]][position[1]]):
            current_position = position
            break
    else:
        raise ValueError("No adjacent pipe found")

    loop = [start_position]

    while True:
        loop.append(current_position)
        pipe = pipe_map[current_position[0]][current_position[1]]
        next_position = get_next_position(current_position, pipe, previous_position)
        if next_position == start_position:
            break

        previous_position = current_position
        current_position = next_position

    return loop


def get_enclosed_tiles(pipe_map: list[list[str]], loop: list[tuple[int, int]]) -> set[tuple[int, int]]:
    enclosed_tiles = set()
    unenclosed_tiles = set()
    all_enclosed_tiles = set()
    max_height = len(pipe_map) -1
    max_width = len(pipe_map[0]) -1
    taken_path = []

    def is_enclosed(position: tuple[int, int]) -> bool:
        taken_path.append(position)
        if position in loop:
            return False
        if position in all_enclosed_tiles:
            return True
        if position in unenclosed_tiles:
            return False
        if position[0] == 0 or position[1] == 0 or position[0] == max_height or position[1] == max_width:
            unenclosed_tiles.add(position)
            return False

        positions_to_check = []
        for transformation in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            adjacent_position = apply_transformation(position, transformation)
            if adjacent_position in unenclosed_tiles:
                unenclosed_tiles.add(position)
                return False
            if adjacent_position in all_enclosed_tiles:
                all_enclosed_tiles.add(position)
                return True
            if adjacent_position in taken_path:
                continue
            if adjacent_position in loop:
                continue
            positions_to_check.append(adjacent_position)

        if positions_to_check:
            for pos in set(positions_to_check):
                if pos not in taken_path:
                    if not is_enclosed(pos):
                        unenclosed_tiles.add(position)
                        return False

        return True

    for i in range(len(pipe_map)):
        for j in range(len(pipe_map[0])):
            if pipe_map[i][j] != "*" and (i, j) not in unenclosed_tiles and is_enclosed((i, j)):
                enclosed_tiles.add((i, j))
                all_enclosed_tiles.add((i, j))
            taken_path.clear()

    return enclosed_tiles


def set_starting_pipe(pipe_map: list[list[str]], loop: list[tuple[int, int]]):
    start_position = loop[0]
    start_pipe = next(
        t for t in TRANSFORMATIONS
        if all(check_adjacency(neighbor, start_position, t) for neighbor in (loop[1], loop[-1]))
    )
    pipe_map[start_position[0]][start_position[1]] = start_pipe


def clean_map(pipe_map: list[list[str]], loop: list[tuple[int, int]]) -> list[list[str]]:
    redrawn_map = []
    for i in range(len(pipe_map)):
        row = []
        for j in range(len(pipe_map[0])):
            if (i, j) in loop:
                row.append(pipe_map[i][j])
            else:
                row.append(".")
        redrawn_map.append(row)
    return redrawn_map


def run1() -> int:
    pipe_map = get_lines_from_file(DAY, type_=list)
    loop = get_loop(pipe_map)
    return (len(loop) + 1) // 2


def run2() -> int:
    pipe_map = get_lines_from_file(DAY, type_=list)
    loop = get_loop(pipe_map)
    set_starting_pipe(pipe_map, loop)
    pipe_map = clean_map(pipe_map, loop)
    max_height = len(pipe_map) - 1
    max_width = len(pipe_map[0]) - 1
    new_map = []

    for row_index, row in enumerate(pipe_map):
        new_row = []
        second_row = []

        for i in range(len(row)):
            if i == max_width:
                new_row.append(row[i])
            elif (
                (row_index, i) in loop
                and (row_index, i + 1) in loop
                and check_adjacency((row_index, i), (row_index, i + 1), row[i + 1])
            ):
                new_row.append(row[i])
                new_row.append("-")
            else:
                new_row.append(row[i])
                new_row.append("*")

            if row_index == max_height:
                continue
            lower_row = pipe_map[row_index + 1]

            if (
                (row_index, i) in loop
                and (row_index + 1, i) in loop
                and check_adjacency((row_index, i), (row_index + 1, i), lower_row[i])
            ):
                second_row.append("|")
                second_row.append("*")
            else:
                second_row.append("*")
                second_row.append("*")

        new_map.append(new_row)

        if second_row:
            new_map.append(second_row)

    start_position = None
    for i in range(len(new_map)):
        j = next((c for c in new_map[i] if c in TRANSFORMATIONS), None)
        if j:
            start_position = (i, new_map[i].index(j))
            break

    loop = get_loop(new_map, start_position=start_position)

    enclosed_tiles = get_enclosed_tiles(new_map, loop)

    return len(enclosed_tiles)
