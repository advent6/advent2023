from typing import Callable

from helpers import get_lines_from_file

DAY = 1

NUMBERS_MAP = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}
REVERSE_NUMBER_MAP = {''.join(reversed(word)): number for word, number in NUMBERS_MAP.items()}


def get_calibration_value(s: str, left_to_right_fun: Callable, right_to_left_fun: Callable) -> int:
    first_digit = next(c for c in left_to_right_fun(s) if c.isdigit())
    last_digit = next(c for c in right_to_left_fun(s) if c.isdigit())
    return int(f"{first_digit}{last_digit}")


def convert_words_to_numbers(line: str, numbers_map: dict[str, str]) -> str:
    for word, number in numbers_map.items():
        if line.startswith(word):
            line = line.replace(word, number, 1)
            break
    if len(line) > 1:
        line = line[0] + convert_words_to_numbers(line[1:], numbers_map)
    return line


def run1() -> int:
    return sum(get_calibration_value(line, lambda s: s, lambda s: reversed(s)) for line in get_lines_from_file(DAY))


def run2() -> int:
    return sum(
        get_calibration_value(
            line,
            lambda s: convert_words_to_numbers(s, NUMBERS_MAP),
            lambda s: convert_words_to_numbers(''.join(reversed(s)), REVERSE_NUMBER_MAP)
        )
        for line in get_lines_from_file(DAY)
    )
