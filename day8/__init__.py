import math
from itertools import cycle

from helpers import get_lines_from_file

DAY = 8


def parse_input(lines: list[str]) -> tuple[tuple[str], dict[str, tuple[str, str]]]:
    turns = tuple(lines[0])
    nodes = {}
    for line in lines[2:]:
        nodes[line[:3]] = (line[7:10], line[12:15])

    return turns, nodes


def get_steps_from_node(
        turns: tuple[str], nodes: dict[str, tuple[str, str]], start_node: str, end_node_suffix: str
) -> int:
    step_count = 0
    instructions = cycle(turns)
    current_node = start_node
    while not current_node.endswith(end_node_suffix):
        instruction = next(instructions)
        current_node = nodes[current_node][0] if instruction == "L" else nodes[current_node][1]
        step_count += 1

    return step_count


def get_steps_from_nodes(
        turns: tuple[str], nodes: dict[str, tuple[str, str]], start_node_suffix: str, end_node_suffix: str
):
    start_nodes = [node for node in nodes if node.endswith(start_node_suffix)]
    cycle_counts = [get_steps_from_node(turns, nodes, node, end_node_suffix) for node in start_nodes]
    return math.lcm(*cycle_counts)


def run1() -> int:
    turns, nodes = parse_input(get_lines_from_file(DAY))
    return get_steps_from_node(turns, nodes, start_node="AAA", end_node_suffix="ZZZ")


def run2() -> int:
    turns, nodes = parse_input(get_lines_from_file(DAY))
    return get_steps_from_nodes(turns, nodes, start_node_suffix="A", end_node_suffix="Z")
