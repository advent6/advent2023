from helpers import get_lines_from_file

DAY = 4


def parse_card(line: str) -> tuple[int, list[int], list[int]]:
    card_id_str, numbers_str = line.split(": ")
    card_id = int(card_id_str.replace("Card ", ""))
    parse_numbers = lambda s: [int(n) for n in s.split(" ") if n]
    winning_numbers, card_numbers = [parse_numbers(s) for s in numbers_str.split(" | ")]
    return card_id, winning_numbers, card_numbers


def get_winning_numbers_count(line: str) -> int:
    _, winning_numbers, card_numbers = parse_card(line)
    matching_numbers = set(winning_numbers).intersection(card_numbers)
    return len(matching_numbers)


def get_card_value(winning_numbers_count: int) -> int:
    return pow(2, winning_numbers_count - 1) if winning_numbers_count else 0


def get_cards_counts(lines: list[str]) -> list[int]:
    line_count = len(lines)
    card_counts = [1] * line_count

    for i, line in enumerate(lines[:line_count - 1]):
        winning_numbers_count = get_winning_numbers_count(line)
        for j in range(winning_numbers_count):
            if (index := i + j + 1) < line_count:
                card_counts[index] += card_counts[i]
            else:
                break

    return card_counts


def run1() -> int:
    return sum(get_card_value(get_winning_numbers_count(line)) for line in get_lines_from_file(DAY))


def run2() -> int:
    return sum(get_cards_counts(get_lines_from_file(DAY)))
