from itertools import chain

from more_itertools import chunked, split_at, windowed

from helpers import get_lines_from_file

DAY = 5


def parse_input(lines: list[str]) -> tuple[list[int], list[dict]]:
    parts = split_at(lines, lambda line: line is None)
    seeds_str = next(parts)[0]
    seeds = [int(s) for s in seeds_str.replace("seeds: ", "").split(" ")]

    maps = []
    for map_name, *map_lines in parts:
        map_params = {"ranges": []}

        map_params["source"], map_params["destination"] = map_name.split()[0].split("-to-", 2)
        for map_line in map_lines:
            map_params["ranges"].append(tuple(int(n) for n in map_line.split(" ")))

        maps.append(map_params)

    return seeds, maps


def select_map(source: str, destination: str, maps: list[dict]) -> dict:
    return next(m for m in maps if m["source"] == source and m["destination"] == destination)


def map_value(value: int, map_ranges: list[tuple[int, int, int]]) -> int:
    for rng in map_ranges:
        destination_range_start, source_range_start, range_length = rng
        if source_range_start <= value < source_range_start + range_length:
            return destination_range_start + value - source_range_start

    return value


def get_location(seed: int, maps: list[dict]) -> int:
    maps_sequence = ["seed", "soil", "fertilizer", "water", "light", "temperature", "humidity", "location"]
    value = seed

    for source, destination in windowed(maps_sequence, 2):
        map_params = select_map(source, destination, maps)
        value = map_value(value, map_params["ranges"])

    return value


def parse_input_with_ranged_seeds(lines: list[str]) -> tuple[list[list[int]], list[dict]]:
    seeds, maps = parse_input(lines)
    return list(chunked(seeds, 2, strict=True)), maps


def map_ranged_value(min_value: int, value_range: int, map_ranges: list[tuple[int, int, int]]) -> list[tuple[int, int]]:
    ranges = []
    max_value = min_value + value_range - 1
    ranges_to_check = [(min_value, max_value)]

    while True:
        if not ranges_to_check:
            break

        min_value, max_value = ranges_to_check.pop()
        for rng in map_ranges:
            destination_range_start, source_range_start, range_length = rng

            if source_range_start <= max_value and source_range_start + range_length - 1 >= min_value:
                intersection_start = max(min_value, source_range_start)
                intersection_end = min(max_value, source_range_start + range_length - 1)
                ranges.append(
                    (
                        destination_range_start + intersection_start - source_range_start,
                        intersection_end - intersection_start + 1,
                    ),
                )

                if intersection_start > min_value:
                    ranges_to_check.append((min_value, intersection_start - 1))
                if intersection_end < max_value:
                    ranges_to_check.append((intersection_end + 1, max_value))

                break
        else:
            ranges.append((min_value, max_value - min_value + 1))

    return ranges


def get_location_ranges(seed_range: list[int], maps: list[dict]) -> list[tuple[int, int]]:
    maps_sequence = ["seed", "soil", "fertilizer", "water", "light", "temperature", "humidity", "location"]
    ranges = [seed_range]

    for source, destination in windowed(maps_sequence, 2):
        map_params = select_map(source, destination, maps)
        next_ranges = []

        for rng in ranges:
            next_ranges.extend(map_ranged_value(rng[0], rng[1], map_params["ranges"]))

        ranges = next_ranges

    return ranges


def run1() -> int:
    seeds, maps = parse_input(get_lines_from_file(DAY))
    locations = [get_location(seed, maps) for seed in seeds]
    return min(locations)


def run2() -> int:
    seed_ranges, maps = parse_input_with_ranged_seeds(get_lines_from_file(DAY))
    location_ranges = chain.from_iterable([get_location_ranges(seed_range, maps) for seed_range in seed_ranges])
    return min(location_ranges, key=lambda rng: rng[0])[0]
