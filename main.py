import importlib
import sys

if __name__ == '__main__':
    day, stage = sys.argv[1:]
    module = importlib.import_module(f"day{day}")
    func = getattr(module, f"run{stage}")
    res = func()
    print(res)
