from helpers import get_lines_from_file, recursionlimit

DAY = 23

TRANSFORMATIONS = ((0, 1), (1, 0), (0, -1), (-1, 0))

HILL_TRANSFORMATIONS = {
    ">": (0, 1),
    "<": (0, -1),
    "^": (-1, 0),
    "v": (1, 0),
}


class Node:
    def __init__(self, value: str, coordinates: tuple[int, int]):
        self.coordinates = coordinates
        self.value = value
        self.next_nodes = set()
        self.visited = False

    def __hash__(self):
        return hash(self.coordinates)


def parse_input(lines: list[str], is_slope_slippery: bool = True) -> list[Node]:
    nodes = {}
    for i, line in enumerate(lines):
        for j, char in enumerate(line):
            if char != "#":
                nodes[(i, j)] = Node(char, (i, j))

    for node in nodes.values():
        transformations = (
            [HILL_TRANSFORMATIONS[node.value]]
            if is_slope_slippery and node.value in HILL_TRANSFORMATIONS
            else TRANSFORMATIONS
        )
        for transformation in transformations:
            new_coordinates = (node.coordinates[0] + transformation[0], node.coordinates[1] + transformation[1])
            if new_coordinates in nodes:
                node.next_nodes.add(nodes[new_coordinates])

    return list(nodes.values())


def get_longest_path(start_node: Node, end_node: Node) -> int:
    longest_path_length = 0

    def step(node: Node, path_length: int):
        nonlocal longest_path_length
        if node == end_node:
            longest_path_length = max(longest_path_length, path_length)
            return

        node.visited = True

        for next_node in node.next_nodes:
            if not next_node.visited:
                step(next_node, path_length + 1)

        node.visited = False

    step(start_node, 0)
    return longest_path_length


def run(is_slope_slippery: bool) -> int:
    nodes = parse_input(get_lines_from_file(DAY), is_slope_slippery=is_slope_slippery)
    start_node = min(nodes, key=lambda node: node.coordinates[0])
    end_node = max(nodes, key=lambda node: node.coordinates[0])
    with recursionlimit(1000000):
        return get_longest_path(start_node, end_node)


def run1() -> int:
    return run(is_slope_slippery=True)


def run2() -> int:
    return run(is_slope_slippery=False)
