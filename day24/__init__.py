from itertools import combinations
from typing import Optional

from helpers import get_lines_from_file

DAY = 24


class Hailstone:
    def __init__(self, point: tuple[float, float, float], velocity: tuple[float, float, float]):
        self.point = point
        self.velocity = velocity

        other_point = (point[0] + velocity[0], point[1] + velocity[1])
        self.slope = (other_point[1] - point[1]) / (other_point[0] - point[0])
        self.intercept = point[1] - self.slope * point[0]

    def get_intersection(self, other: "Hailstone") -> Optional[tuple[float, float]]:
        if self.slope == other.slope:
            return None

        x = (other.intercept - self.intercept) / (self.slope - other.slope)
        y = self.slope * x + self.intercept
        return x, y

    def check_past(self, point: tuple[int, int]):
        for i in range(2):
            if self.velocity[i] > 0 and point[i] < self.point[i] or self.velocity[i] < 0 and point[i] > self.point[i]:
                return True
        return False

    def __repr__(self):
        return f"Hailstone(point={self.point}, velocity={self.velocity})"


def parse_input(lines: list[str]) -> list[Hailstone]:
    hailstones = []
    for line in lines:
        point_str, velocity_str = line.split(" @ ")
        point = tuple(map(float, point_str.split(", ", 3)))
        velocity = tuple(map(float, velocity_str.split(", ", 3)))
        hailstones.append(Hailstone(point, velocity))

    return hailstones


def run1() -> int:
    hailstones = parse_input(get_lines_from_file(DAY, file_name="input.txt"))
    min_coordinate = 200000000000000
    max_coordinate = 400000000000000
    res = 0
    for h1, h2 in combinations(hailstones, 2):
        intersection = h1.get_intersection(h2)
        if (
                intersection is not None
                and all(min_coordinate <= c <= max_coordinate for c in intersection)
                and not any(h.check_past(intersection) for h in (h1, h2))
        ):
            res += 1

    return res
