import math
from enum import Enum
from typing import Optional

from helpers import get_lines_from_file

DAY = 20


class Pulse(str, Enum):
    HIGH = "H"
    LOW = "L"


class ModuleBase:
    def __init__(self, key: str, destinations: list[str]):
        super().__init__()
        self.key = key
        self.destinations = destinations
        self.pulse_counter = {Pulse.LOW: 0, Pulse.HIGH: 0}
        #---part 2---
        self.sent_pulse_counter = {Pulse.LOW: 0, Pulse.HIGH: 0, None: 0}
        self.inputs = []

    def register_input(self, key: str):
        self.inputs.append(key)

    def transform_pulse(self, from_key: str, pulse: Pulse) -> Optional[Pulse]:
        raise NotImplementedError

    def receive_pulse(self, from_key: str, pulse: Pulse) -> list[tuple[str, Pulse]]:
        new_pulse = self.transform_pulse(from_key, pulse)

        self.pulse_counter[pulse] += 1
        self.sent_pulse_counter[new_pulse] += 1

        return [(dest, new_pulse) for dest in self.destinations] if new_pulse is not None else []


class FlipFlopModule(ModuleBase):
    def __init__(self, key: str, destinations: list[str]):
        super().__init__(key, destinations)
        self.is_on = False

    def transform_pulse(self, from_key: str, pulse: Pulse) -> Optional[Pulse]:
        if pulse == Pulse.HIGH:
            return None

        new_pulse = Pulse.LOW if self.is_on else Pulse.HIGH
        self.is_on = not self.is_on
        return new_pulse


class ConjunctionModule(ModuleBase):
    def __init__(self, key: str, destinations: list[str]):
        super().__init__(key, destinations)
        self.recent_inputs = {}

    def register_input(self, key: str):
        super().register_input(key)
        self.recent_inputs[key] = Pulse.LOW

    def transform_pulse(self, from_key: str, pulse: Pulse) -> Optional[Pulse]:
        self.recent_inputs[from_key] = pulse
        return Pulse.HIGH if any(p == Pulse.LOW for p in self.recent_inputs.values()) else Pulse.LOW


class BroadcasterModule(ModuleBase):
    def transform_pulse(self, from_key: str, pulse: Pulse) -> Optional[Pulse]:
        return pulse


class UntypedModule(ModuleBase):
    def transform_pulse(self, from_key: str, pulse: Pulse) -> Optional[Pulse]:
        return None


def setup_modules(lines: list[str]) -> dict[str, ModuleBase]:
    modules = {}
    for line in lines:
        module_name, destinations_str = line.split(" -> ")
        destinations = destinations_str.split(", ")
        if module_name == "broadcaster":
            kls = BroadcasterModule
        elif module_name.startswith("&"):
            kls = ConjunctionModule
            module_name = module_name[1:]
        elif module_name.startswith("%"):
            kls = FlipFlopModule
            module_name = module_name[1:]
        else:
            raise ValueError(f"Unknown module type {module_name}")

        modules[module_name] = kls(module_name, destinations)

    for module in list(modules.values()):
        for destination in module.destinations:
            if destination not in modules:
                modules[destination] = UntypedModule(destination, [])
            modules[destination].register_input(module.key)

    return modules


def push_button(modules: dict[str, ModuleBase]):
    pulses_to_send = [("button", "broadcaster", Pulse.LOW)]

    while pulses_to_send:
        from_key, key, pulse = pulses_to_send.pop(0)
        for dest, new_pulse in modules[key].receive_pulse(from_key, pulse):
            pulses_to_send.append((key, dest, new_pulse))


def get_pulse_score(modules: dict[str, ModuleBase]) -> int:
    return (
            sum(
                modules[key].pulse_counter[Pulse.HIGH] for key in modules
            ) * sum(
                modules[key].pulse_counter[Pulse.LOW] for key in modules
            )
    )


def run1() -> int():
    lines = get_lines_from_file(DAY, file_name="input.txt")
    modules = setup_modules(lines)
    for _ in range(1000):
        push_button(modules)
    return get_pulse_score(modules)


def run2():
    lines = get_lines_from_file(DAY, file_name="input.txt")
    modules = setup_modules(lines)
    high_signals = {key: None for key in modules[modules["rx"].inputs[0]].inputs}

    i = 0
    while not all(high_signals.values()):
        i += 1
        push_button(modules)
        for key in high_signals:
            if high_signals[key] is None and modules[key].sent_pulse_counter[Pulse.HIGH]:
                high_signals[key] = i

    return math.prod(high_signals.values())
